{%- set server_name = salt['pillar.get']('openvpn:specs:server') %}
{%- set ca_name = salt['pillar.get']('openvpn:specs:ca_name') %}

{%- for clients in salt['pillar.get']('openvpn:specs:client') %}
  {%- set client_name = clients %}

/etc/openvpn/client/{{ client_name }}/{{ server_name }}.conf:
  file.managed:
    - source: salt://openvpn/files/client.conf.j2
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja
    - context:
      client_name:     {{ client_name }}

/etc/openvpn/client/{{ client_name }}/ca.pem:
  file.managed:
    - source: salt://{{ ca_name }}/ca.pem
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - defaults:
        custom_var: "default value"
        other_var: 123

/etc/openvpn/client/{{ client_name }}/ta.key:
  file.managed:
    - source: /etc/openvpn/server/ta.key
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - defaults:
        custom_var: "default value"
        other_var: 123

{%- endfor %}


