include:
  - iptables

openvpn_server_service:
  service.running:
    - name: openvpn@server
    - enable: True
    - watch:
      - file: /etc/openvpn/server.conf 
#      - file: /etc/openvpn/server/*
