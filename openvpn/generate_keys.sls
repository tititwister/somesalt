{% set dh_size = salt['pillar.get']('openvpn:crypto:dh') %}

generate_dh:
  cmd.run:
    - name: "openssl dhparam -out dh.pem {{ dh_size }}"
    - cwd: /etc/openvpn/server
    - umask: 377
    - unless: ls /etc/openvpn/server/dh.pem

generate_ta_key:
  cmd.run:
    - name: "openvpn --genkey --secret ta.key"
    - cwd: /etc/openvpn/server
    - umask: 377
    - unless: ls /etc/openvpn/server/ta.key
