/etc/openvpn/server.conf:
  file.managed:
    - source: salt://openvpn/files/server.conf.j2
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja
    - defaults:
        custom_var: "default value"
        other_var: 123

include:
    - forwarding
