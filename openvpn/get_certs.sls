{% set ca_name = salt['pillar.get']('openvpn:specs:ca_name') %}
{% set server_name = salt['pillar.get']('openvpn:specs:server') %}

/etc/openvpn/server/ca.pem:
  file.managed:
    - source: salt://{{ ca_name }}/ca.pem
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - defaults:
        custom_var: "default value"
        other_var: 123

{%- for file in ['.pem', '-key.pem'] %}
  {%- set filename = file %}

/etc/openvpn/server/{{ server_name }}{{ filename }}:
  file.managed:
    - source: salt://{{ ca_name }}/server/{{ server_name }}/{{ server_name }}{{ filename }}
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - defaults:
        custom_var: "default value"
        other_var: 123
{%- endfor %}

{%- for client in salt['pillar.get']('openvpn:specs:client') %}
  {%- set client_name = client %}
  {%- for file in ['.pem', '-key.pem'] %}
    {%- set filename = file %}

/etc/openvpn/client/{{ client_name }}/{{ client_name }}{{ filename }}:
  file.managed:
    - source: salt://{{ ca_name }}/client/{{ client_name }}/{{ client_name }}{{ filename }}
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - defaults:
        custom_var: "default value"
        other_var: 123
  {%- endfor %}
{%- endfor %}
