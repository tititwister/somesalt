bind_service:
  service.running:
    - name: named
    - enable: True
    - watch:
      - file: /var/named/forward.home
      - file: /var/named/reverse.home
      - file: /etc/named.conf
