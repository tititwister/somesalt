/etc/named.conf:
  file.managed:
    - source: salt://bind/files/named.conf.j2
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja
    - defaults:
        custom_var: "default value"
        other_var: 123

/var/named/forward.home:
  file.managed:
    - source: salt://bind/files/forward.home.j2
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja
    - defaults:
        custom_var: "default value"
        other_var: 123

/var/named/reverse.home:
  file.managed:
    - source: salt://bind/files/reverse.home.j2
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja
    - defaults:
        custom_var: "default value"
        other_var: 123
