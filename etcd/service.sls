etcd_systemctl_reload:
  module.run:
    - name: service.systemctl_reload 
    - onchanges:
      - file: /etc/systemd/system/etcd.service

service_etcd:
    service.running:
    - name: etcd
    - enable: True
    - watch:
      - file: /etc/systemd/system/etcd.service
