{%- set etcd_version = salt['pillar.get']('etcd:version') %}
{%- set files = ['etcd', 'etcdctl'] %}

install_etcd:
  archive.extracted:
    - name: /opt
    - source: "https://github.com/coreos/etcd/releases/download/v{{ etcd_version }}/etcd-v{{ etcd_version }}-linux-amd64.tar.gz"
    - skip_verify: True
    - unless:
      - /opt/etcd-v3.4.0-linux-amd64/etcd --version

{%- for file in files %}

{{ file }}_symlink:
  file.symlink:
    - name: /usr/local/bin/{{ file }}
    - target: /opt/etcd-v{{ etcd_version }}-linux-amd64/{{ file }}

{%- endfor %}
