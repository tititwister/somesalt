{%- set cn = salt['pillar.get']('etcd:CN') %}
{%- set cert_path = salt['pillar.get']('etcd:cert_path')%}

/etc/etcd/ca.pem:
  file.managed:
    - source: salt://{{ cn }}/ca.pem
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True

/etc/etcd/etcd.pem:
  file.managed:
    - source: salt://{{ cn }}{{ cert_path }}.pem
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True

/etc/etcd/etcd-key.pem:
  file.managed:
    - source: salt://{{ cn }}{{ cert_path }}-key.pem
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True

/etc/systemd/system/etcd.service:
  file.managed:
    - source: salt://etcd/files/etcd.service.j2
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja
