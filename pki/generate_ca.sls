{% set ca_name = salt['pillar.get']('pki:CA:CN') %}
#{% set base_path = salt['pillar.get']('pki:CA:path') %}
{% set base_path = "/srv/salt-secret" %}

{{ base_path }}/{{ ca_name }}/ca-csr.json:
  file.managed:
    - source: salt://pki/files/ca-csr.json.j2
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja
    - defaults:
        custom_var: "default value"
        other_var: 123

{{ base_path }}/{{ ca_name }}/ca-config.json:
  file.managed:
    - source: salt://pki/files/ca-config.json.j2
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja
    - defaults:
        custom_var: "default value"
        other_var: 123

generate_root_ca_{{ ca_name }}:
  cmd.run:
    - name: "cfssl gencert -initca ca-csr.json | cfssljson -bare ca"
    - cwd: {{ base_path }}/{{ ca_name }}
    - umask: 377
    - onchanges:
      - file: {{ base_path }}/{{ ca_name }}/ca-csr.json
