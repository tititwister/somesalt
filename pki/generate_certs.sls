{%- set base_path = "/srv/salt-secret" %}
{%- set ca_name = salt['pillar.get']('pki:CA:CN') %}
{%- set ca_path = base_path ~ '/' ~ ca_name %}

{%- for key_names, value_names in salt['pillar.get']('pki:certs').iteritems() %}
  {%- set cert_cn = key_names %}
  {%- set cert_profile = "server" %}
  {%- set cert_profile = value_names['profile'] %}

  {%- set cert_path = ca_path ~ '/' ~ cert_profile ~ '/' ~ cert_cn %}

{{ cert_path }}/{{ cert_cn }}-csr.json:
  file.managed:
    - source: salt://pki/files/certs-csr.json.j2
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja
    - context:
      cert_cn:     {{ cert_cn }}

generate_{{ cert_cn }}_certs:
  cmd.run:
    - name: "cfssl gencert -ca={{ ca_path }}/ca.pem -ca-key={{ ca_path }}/ca-key.pem -config={{ ca_path }}/ca-config.json -profile={{ cert_profile }} {{ cert_cn }}-csr.json | cfssljson -bare {{ cert_cn }}"
    - cwd: {{ cert_path }}
    - umask: 377
    - onchanges:
      - file: {{ ca_path }}/ca-csr.json
      - file: {{ cert_path }}/{{ cert_cn }}-csr.json
{%- endfor %}
