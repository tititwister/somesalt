{% set cfssl_version = salt['pillar.get']('pki:cfssl:version') %}
{% set cfssl_hash = salt['pillar.get']('pki:cfssl:hash') %}

{% set cfssljson_version = salt['pillar.get']('pki:cfssljson:version') %}
{% set cfssljson_hash = salt['pillar.get']('pki:cfssljson:hash') %}

install_cfssl:
  file.managed:
    - name: /usr/local/bin/cfssl
    - source: "https://pkg.cfssl.org/R{{ cfssl_version }}/cfssl_linux-amd64"
    - source_hash: "{{ cfssl_hash }}"
    - mode: 0555


install_cfssljson:
  file.managed:
    - name: /usr/local/bin/cfssljson
    - source: "https://pkg.cfssl.org/R{{ cfssljson_version }}/cfssljson_linux-amd64"
    - source_hash: "{{ cfssljson_hash }}"
    - mode: 0555

install_openvpn:
  pkg.installed:
    - pkgs:
      - openssl
