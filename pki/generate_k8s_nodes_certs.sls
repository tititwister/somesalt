{%- set ca_name = salt['pillar.get']('pki:CA:CN') %}
{%- set cert_profile = salt['pillar.get']('pki:certs:kubelet:profile') %}
{%- set ca_path = "/srv/salt-secret" ~ '/' ~ ca_name %}
{%- set cert_path = ca_path ~ '/' ~ cert_profile ~ '/nodes' %}

{%- for node in salt['mine.get']("*k8s-*er*", "network.ip_addrs") %}

{%- set node_ip = salt['mine.get'](node , "network.ip_addrs")[node][0] %}

{{ cert_path }}/{{ node }}-csr.json:
  file.managed:
    - source: salt://pki/files/k8s-node-csr.json.j2
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja
    - context:
      node_name:     {{ node }}
      node_ip:       {{ node_ip }}

generate_{{ node }}_certs:
  cmd.run:
    - name: "cfssl gencert -ca={{ ca_path }}/ca.pem -ca-key={{ ca_path }}/ca-key.pem -config={{ ca_path }}/ca-config.json -profile={{ cert_profile }} {{ node }}-csr.json | cfssljson -bare {{ node }}"
    - cwd: {{ cert_path }}
    - umask: 377
    - onchanges:
      - file: {{ ca_path }}/ca-csr.json
      - file: {{ cert_path }}/{{ node }}-csr.json
{%- endfor %}
