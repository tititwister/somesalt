include:
  - pki.install
  - pki.generate_ca
  - pki.generate_certs
  - pki.generate_k8s_nodes_certs
