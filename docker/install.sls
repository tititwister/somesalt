install_glusterfs_repo:
  cmd.run:
    - name: "yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo"
    - umask: 377
    - unless: yum-config-manager | grep docker-ce

install_docker:
  cmd.run:
    - name: "yum install -y docker-ce-18.06.2.ce"
    - umask: 377
    - unless: rpm -qa | grep docker-ce-18.06.2
