docker_systemctl_reload:
  module.run:
    - name: service.systemctl_reload
    - onchanges:
      - file: /etc/docker/daemon.json

docker_service:
  service.running:
    - name: docker
    - enable: True
    - watch:
      - file: /etc/docker/daemon.json
