/etc/docker/daemon.json:
  file.managed:
    - source: salt://docker/files/daemon.json.j2
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja

/etc/systemd/system/docker.service.d:
  file.directory:
    - user: root
    - group: root
    - mode: 755
    - makedirs: True
