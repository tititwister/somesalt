install_gns3_dependencies:
  pkg.installed:
    - pkgs:
      - gcc 
      - gcc-c++ 
      - openssl-devel 
      - bzip2-devel
      - elfutils-libelf-devel
      - libpcap-devel
      - cmake
      - glibc-static
      - qemu
      - telnet
      - putty
      - qt5-qtbase 
      - qt5-qtbase-devel 
      - qt5-qtsvg 
      - qt5-qtsvg-devel

install_python3_dependencies:
  cmd.run:
    - name: "yum install -y python36 python36-devel python36-setuptools"
    - umask: 377
    - unless: rpm -qa | grep python3-setuptools

install_pip3:
  cmd.run:
    - name: "curl https://bootstrap.pypa.io/get-pip.py | python3.6"
    - umask: 377
    - unless: rpm -qa | grep python3-pip

{%- for package in ["gns3-server", "sip", "pyqt5"] %}

install_{{ package }}:
  cmd.run:
    - name: "pip3.6 install {{ package }}"
    - umask: 377
    - unless: pip3.6 show {{ package }} | grep Version

{%- endfor %}

install_dynamips:
  cmd.run:
    - name: "git clone https://github.com/GNS3/dynamips.git /opt"
    - umask: 377
    - unless: ls -la /opt
