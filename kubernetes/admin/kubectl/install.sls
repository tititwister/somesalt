{% set kubectl = salt['pillar.get']('kubernetes:kubectl') %}

install_kubectl:
  file.managed:
    - name: /usr/local/bin/kubectl
    - source: "https://storage.googleapis.com/kubernetes-release/release/{{ kubectl['version'] }}/bin/linux/amd64/kubectl"
    - mode: 0755
    - source_hash: {{ kubectl['hash'] }}
