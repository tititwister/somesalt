###################################################
############### KUBE-APISERVER-TO-KUBELET #########
###################################################

/etc/kubernetes/deploy/kube-apiserver-to-kubelet/cluster_role.yaml:
  file.managed:
    - source: salt://kubernetes/files/deploy/kube-apiserver-to-kubelet/cluster_role.yaml.j2
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja

/etc/kubernetes/deploy/kube-apiserver-to-kubelet/cluster_role_binding.yaml:
  file.managed:
    - source: salt://kubernetes/files/deploy/kube-apiserver-to-kubelet/cluster_role_binding.yaml.j2
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja

##################################################
##################@ KUBE-ROUTER ##################
##################################################

/etc/kubernetes/deploy/kube-router/generic-kuberouter-all-features.yaml:
  file.managed:
    - source: salt://kubernetes/files/deploy/kube-router/generic-kuberouter-all-features.yaml.j2
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja
