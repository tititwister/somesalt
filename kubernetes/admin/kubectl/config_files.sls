include:
  - kubernetes.ca

{%- set ca_name = salt['pillar.get']('kubernetes:CN') %}

/root/.kube/config:
  file.managed:
    - source: salt://kubernetes/files/kubeconfig.j2
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja
    - context:
      file_name:     admin

/etc/kubernetes/admin.pem:
  file.managed:
    - source: salt://{{ ca_name }}/kubernetes/admin/admin.pem
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja

/etc/kubernetes/admin-key.pem:
  file.managed:
    - source: salt://{{ ca_name }}/kubernetes/admin/admin-key.pem
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja

