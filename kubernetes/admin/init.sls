include:
  - kubernetes.admin.kubectl.install
  - kubernetes.admin.kubectl.config_files
  - kubernetes.admin.encryption
  - kubernetes.admin.kubectl.deploy_files
