{% set key = salt['cmd.run']('head -c 32 /dev/urandom ')  %}

/srv/salt-secret/kubernetes_encryption/encryption-config.yaml:
  file.managed:
    - source: salt://kubernetes/files/encryption-config.yaml.j2
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja
    - context:
      key:     {{ salt['hashutil.base64_b64encode'](key) }}
    - unless:
      - "test -f /srv/salt-secret/kubernetes_encryption/encryption-config.yaml"
