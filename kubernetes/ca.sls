{%- set ca_name = salt['pillar.get']('kubernetes:CN') %}

/etc/kubernetes/ca.pem:
  file.managed:
    - source: salt://{{ ca_name }}/ca.pem
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja
