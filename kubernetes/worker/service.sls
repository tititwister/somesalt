{%- set kube_proxy = salt['pillar.get']('kubernetes:use-kube-proxy') %}

{%- if kube_proxy == "true" %}
  {%- set packages = ['kube-proxy', 'kubelet'] %}
{%- else %}
  {%- set packages = ['kubelet'] %}
{% endif %}

worker_service_systemctl_reload:
  module.run:
    - name: service.systemctl_reload
    - onchanges:
      - file: /etc/systemd/system/kubelet.service

{%- for package in packages %}

service_{{ package }}:
    service.running:
    - name: {{ package }}
    - enable: True
    - watch:
      - file: /etc/systemd/system/{{ package }}.service

{%- endfor %}

