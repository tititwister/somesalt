include:
  - kubernetes.worker.config_files
  - kubernetes.worker.install
  - kubernetes.worker.cni
  - kubernetes.worker.service
