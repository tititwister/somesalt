{% set version = salt['pillar.get']('kubernetes:cni:version') %}

install_cni:
  archive.extracted:
    - name: /opt/cni/bin
    - source: "https://github.com/containernetworking/plugins/releases/download/v{{ version }}/cni-plugins-linux-amd64-v{{ version }}.tgz"
    - skip_verify: True
    - if_missing: /opt/cni/bin

/etc/cni/net.d:
  file.directory:
    - user: root
    - group: root
    - mode: 755
    - makedirs: True
