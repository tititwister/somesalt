include:
  - kubernetes.ca

{%- set ca_name = salt['pillar.get']('kubernetes:CN') %}
{%- set hostname = salt['grains.get']('host') %}

/etc/kubernetes/kubelet.kubeconfig: # replace the hostname by kubelet
  file.managed:
    - source: salt://kubernetes/files/kubeconfig.j2
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja
    - context:
      file_name:     {{ hostname }}

/etc/kubernetes/{{ hostname }}.pem:
  file.managed:
    - source: salt://{{ ca_name }}/kubernetes/nodes/{{ hostname }}.pem
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True

/etc/kubernetes/{{ hostname }}-key.pem:
  file.managed:
    - source: salt://{{ ca_name }}/kubernetes/nodes/{{ hostname }}-key.pem
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
