{%- set k8s_version = salt['pillar.get']('kubernetes:version') %}
{%- set host_ip = salt['grains.get']('ip4_interfaces:eth0')[0] %}
{%- set kube_proxy = salt['pillar.get']('kubernetes:use-kube-proxy') %}

{%- if kube_proxy == "true" %}
  {%- set packages = ['kube-proxy', 'kubelet'] %}
{%- else %}
  {%- set packages = ['kubelet'] %}
{% endif %}
{%- for package in packages %}

install_{{ package }}:
  file.managed:
    - name: /usr/local/bin/{{ package }}
    - source: "https://storage.googleapis.com/kubernetes-release/release/v{{ k8s_version }}/bin/linux/amd64/{{ package }}"
    - skip_verify: True
    - mode: 0755

/etc/systemd/system/{{ package }}.service:
  file.managed:
    - source: salt://kubernetes/files/kube.service.j2
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja
    - context:
      package:     {{ package }}
      host_ip:     {{ host_ip }}

{%- endfor %}
