{%- set packages = ['kube-apiserver', 'kube-controller-manager', 'kube-scheduler'] %}

control_plane_systemctl_reload:
  module.run:
    - name: service.systemctl_reload
    - onchanges:
      - file: /etc/systemd/system/kube-scheduler.service
      - file: /etc/systemd/system/kube-apiserver.service
      - file: /etc/systemd/system/kube-controller-manager.service

{%- for package in packages %}

service_{{ package }}:
    service.running:
    - name: {{ package }}
    - enable: True
    - watch:
      - file: /etc/systemd/system/{{ package }}.service

{%- endfor %}
