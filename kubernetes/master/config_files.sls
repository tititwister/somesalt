include:
  - kubernetes.ca

{%- set file_names = ["kube-proxy", "kube-scheduler", "kube-controller-manager", "service-account"] %}
{%- set ca_name = salt['pillar.get']('kubernetes:CN') %}

{%- for file_name in file_names %}

  {%- if file_name == "service-account" %}
  {%- else  %}

/etc/kubernetes/{{ file_name }}.kubeconfig:
  file.managed:
    - source: salt://kubernetes/files/kubeconfig.j2
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja
    - context:
      file_name:     {{ file_name }}

  {%- endif %}

/etc/kubernetes/{{ file_name }}.pem:
  file.managed:
    - source: salt://{{ ca_name }}/kubernetes/{{ file_name }}/{{ file_name }}.pem
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True

/etc/kubernetes/{{ file_name }}-key.pem:
  file.managed:
    - source: salt://{{ ca_name }}/kubernetes/{{ file_name }}/{{ file_name }}-key.pem
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True

{%- endfor %}
