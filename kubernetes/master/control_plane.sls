{%- set k8s_version = salt['pillar.get']('kubernetes:version') %}
{%- set packages = ['kube-apiserver', 'kube-controller-manager', 'kube-scheduler'] %}
{%- set host_ip = salt['grains.get']('ip4_interfaces:eth0')[0] %}

{%- for package in packages %}

install_{{ package }}:
  file.managed:
    - name: /usr/local/bin/{{ package }}
    - source: "https://storage.googleapis.com/kubernetes-release/release/v{{ k8s_version }}/bin/linux/amd64/{{ package }}"
    - skip_verify: True
    - mode: 0755

/etc/systemd/system/{{ package }}.service:
  file.managed:
    - source: salt://kubernetes/files/kube.service.j2
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja
    - context:
      package:     {{ package }}
      host_ip:     {{ host_ip }}

{%- endfor %}

{%- set cn = salt['pillar.get']('kubernetes:CN') %}

/etc/kubernetes/kubernetes.pem:
  file.managed:
    - source: salt://{{ cn }}/kubernetes/kubernetes/kubernetes.pem
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True

/etc/kubernetes/kubernetes-key.pem:
  file.managed:
    - source: salt://{{ cn }}/kubernetes/kubernetes/kubernetes-key.pem
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True

/etc/kubernetes/encryption-config.yaml:
  file.managed:
    - source: salt://kubernetes_encryption/encryption-config.yaml
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True

