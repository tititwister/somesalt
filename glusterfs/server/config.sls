{% if salt['grains.get']('host') == salt['pillar.get']('glusterfs:peers')[0] %}

wait_for_peer_probe:
  cmd.run:
    - name: "sleep 10s"
    - umask: 377
    - unless: gluster peer status | grep Hostname

  {% for peers in salt['pillar.get']('glusterfs:peers') %}
    {%- set peer = peers %}

peer_probe_{{ peer }}:
  cmd.run:
    - name: "gluster peer probe {{ peer }}"
    - umask: 377
    - unless: gluster peer status | grep {{ peer }}

  {%- endfor %}

  {% for volumes in salt['pillar.get']('glusterfs:volumes') %}
    {%- set volume = volumes %}

{{ volume['folder'] }}:
  file.directory:
    - user: root
    - group: root
    - mode: 755
    - makedirs: True

create_volume_{{ volume['name'] }}:
  cmd.run:
    - name: "gluster volume create {{ volume['name'] }} {{ volume['type'] }} {{ volume['arg_cmd'] }}"
    - umask: 377
    - unless: gluster volume info | grep {{ volume['name'] }}

start_volume_{{ volume['name'] }}:
  cmd.run:
    - name: gluster volume start {{ volume['name'] }}
    - umask: 377
    - unless: gluster volume info {{ volume['name'] }} | grep Started

  {%- endfor %}
{% endif %}
