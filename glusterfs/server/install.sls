install_glusterfs_repo:
  cmd.run:
    - name: "yum install -y centos-release-gluster"
    - umask: 377
    - unless: rpm -qa | grep centos-release-gluster 


install_glusterfs_package:
  pkg.installed:
    - pkgs:
      - glusterfs
      - glusterfs-cli
      - glusterfs-libs
      - glusterfs-server 
