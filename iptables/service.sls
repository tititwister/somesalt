{%- set rules_file = '/etc/iptables/rules.v4' %}

flush_filter_table:
    iptables.flush:
        - table: filter
        - family: ipv4
        - onchanges:
            - file: "{{ rules_file }}"

flush_nat_table:
    iptables.flush:
        - table: nat
        - family: ipv4
        - onchanges:
            - file: "{{ rules_file }}"

create_rules:
    cmd.run:
        - name: "/usr/sbin/iptables-restore < {{ rules_file }}"
        - onchanges:
            - file: "{{ rules_file }}"
