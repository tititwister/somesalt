/etc/iptables/rules.v4:
  file.managed:
    - source: salt://iptables/files/rules.v4.j2
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja
    - defaults:
        custom_var: "default value"
        other_var: 123

