{%- set dns = salt['pillar.get']('network:dns') %}
{%- set domain = salt['pillar.get']('network:domain') %}

/etc/sysconfig/network-scripts/ifcfg-eth0:
  file.append:
    - text: 
      - 'DNS2="10.111.100.53"'
      - 'DOMAIN="home.local"'

network_service:
  service.running:
    - name: network
    - enable: True
    - watch:
      - file: /etc/sysconfig/network-scripts/ifcfg-eth0
