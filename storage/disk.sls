{%- for disk_src, disk_values in salt['pillar.get']('storage:disk').iteritems() %}
  {%- set disk = disk_src %}
  {%- set mount_point = disk_values['mount_point'] %}
  {%- set fs = disk_values['fs'] %}

partition_{{ disk }}:
  blockdev.formatted:
    - name: {{ disk }}
    - fs_type: {{ fs }}
    - force: True

mount_{{ disk }}_in_{{ mount_point }}:
  mount.mounted:
    - name: {{ mount_point }}
    - device: {{ disk }}
    - fstype: {{ fs }}
    - mkmnt: True
    - opts:
      - defaults
    - failhard: True

{%- endfor %}
