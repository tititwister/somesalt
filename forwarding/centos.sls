/etc/sysctl.d/50-sysctl.conf:
  file.managed:
    - source: salt://forwarding/files/50-sysctl.conf.j2
    - user: root
    - group: root
    - mode: 644
    - attrs: ai
    - makedirs: True
    - template: jinja
    - defaults:
        custom_var: "default value"
        other_var: 123

eable_forwarding:
  cmd.run:
    - name: "/sbin/sysctl -p /etc/sysctl.d/50-sysctl.conf"
    - umask: 377
    - onchanges:
      - file: /etc/sysctl.d/50-sysctl.conf
