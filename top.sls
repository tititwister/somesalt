base:
  '*':
    - network

  '*salt*':
    - git
    - pki
    - kubernetes.admin

  '*dns*':
    - bind

  '*gns3*':
    - git
    - gns3

  '*ovpn*':
    - openvpn

  '*gluster*':
    - glusterfs.server

#  '*k8s-lb*':
#    - haproxy

#  '*k8s-master*':
#    - etcd
#    - docker
#    - kubernetes.master
#    - kubernetes.worker

#  '*k8s-worker*':
#    - git
#    - docker
#    - kubernetes.worker
